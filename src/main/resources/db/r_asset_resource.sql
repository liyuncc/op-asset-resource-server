CREATE TABLE R_ASSET_RESOURCE
(
    resource_id           BIGINT PRIMARY KEY,
    resource_name         VARCHAR(128),
    resource_storage_path VARCHAR(256),
    owner_id              BIGINT,
    owner_type            INTEGER,
    mime_type             VARCHAR(128),
    checksum              VARCHAR(128),
    version               VARCHAR(64)
);