package com.only4play.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceUploadDto
 * @brief AssetResourceUploadDto
 * @details AssetResourceUploadDto
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class AssetResourceUploadDto {

    @NotNull
    private MultipartFile resource;

    private Long ownerId;

    private Integer ownerType;
}
