package com.only4play.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.only4play.model.entity.AssetResourceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceMapper
 * @brief AssetResourceMapper
 * @details AssetResourceMapper
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */
@Mapper
public interface AssetResourceMapper extends BaseMapper<AssetResourceEntity> {
}
