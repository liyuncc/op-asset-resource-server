package com.only4play.vo;

import lombok.*;

import java.io.File;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceDownloadVo
 * @brief AssetResourceDownloadVo
 * @details AssetResourceDownloadVo
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class AssetResourceDownloadVo {

    private Long resourceId;

    private String resourceName;

    private String mimeType;

    private File resource;

    private Long ownerId;

    private Integer ownerType;
}
