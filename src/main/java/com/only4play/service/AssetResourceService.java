package com.only4play.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.only4play.config.AssetResourceConfigProperties;
import com.only4play.dto.AssetResourceUploadDto;
import com.only4play.model.entity.AssetResourceEntity;
import com.only4play.vo.AssetResourceDownloadVo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceService
 * @brief AssetResourceService
 * @details AssetResourceService
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */
public interface AssetResourceService extends IService<AssetResourceEntity> {

    String upload(AssetResourceUploadDto uploadDto);

    AssetResourceDownloadVo download(Long resourceId);

}
