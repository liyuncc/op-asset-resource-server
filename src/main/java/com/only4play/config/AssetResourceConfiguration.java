package com.only4play.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceConfiguration
 * @brief AssetResourceConfiguration
 * @details AssetResourceConfiguration
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */

@Configuration
@EnableConfigurationProperties(value = AssetResourceConfigProperties.class)
public class AssetResourceConfiguration {


}
