package com.only4play.config;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.file.Paths;
import java.util.Objects;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceConfigProperties
 * @brief AssetResourceConfigProperties
 * @details AssetResourceConfigProperties
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */

@NoArgsConstructor
@Getter
@Setter
@ConfigurationProperties(prefix = "op.asset-resource")
public class AssetResourceConfigProperties {
    private Long defaultLimitSize;
    private String storageRoot;
    private String storageRule;

    private static final Long SIZE = 1024L * 1024L * 10;
    private static final String ROOT = SystemUtils.getUserDir().toString();
    private static final String RULE = "time";

    @PostConstruct
    private void init() {
        if (Objects.isNull(defaultLimitSize)) {
            defaultLimitSize = SIZE;
        }
        if (Objects.isNull(storageRoot)) {
            storageRoot = Paths.get(ROOT, "resource_storage").toString();
        }
        if (Objects.isNull(storageRule)) {
            storageRule = RULE;
        }
    }
}
