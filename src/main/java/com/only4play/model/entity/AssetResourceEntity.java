package com.only4play.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

/**
 * @author liyuncong
 * @version 1.0
 * @file AssetResourceEntity
 * @brief AssetResourceEntity
 * @details AssetResourceEntity
 * @date 2023-12-22
 *
 * Edit History
 * ----------------------------------------------------------------------------
 * DATE                     NAME               DESCRIPTION
 * 2023-12-22               liyuncong          Created
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@TableName("R_ASSET_RESOURCE")
public class AssetResourceEntity {

    @TableId("resource_id")
    private Long resourceId;
    @TableField("resource_name")
    private String resourceName;
    @TableField("resource_storage_path")
    private String resourceStoragePath;
    @TableField("owner_id")
    private Long ownerId;
    @TableField("owner_type")
    private Integer ownerType;
    @TableField("mime_type")
    private String mimeType;
    @TableField("checksum")
    private String checksum;
    @TableField("version")
    private String version;
}
